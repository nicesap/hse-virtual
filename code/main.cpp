#include "cat.h"
#include "tiger.h"
#include "man.h"
#include "woman.h"

int main()
{
    {
        Cat cat{"Nagibator666"};
        cat.interact();
    }

    {
        Man man;
        man.interact();
    }

    {
        Woman woman;
        woman.interact();
    }

    {
        Tiger tiger{"LazyOne"};
        tiger.interact();
        Cat clonedTiger = tiger;
        clonedTiger.interact();
    }
}
