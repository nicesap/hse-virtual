#pragma once

#include "cat.h"

class Tiger final : public Cat
{
public:
    explicit Tiger(std::string name);

    std::string breed() const override;
};
