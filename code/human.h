#pragma once

#include "animal.h"

class Human : private Animal
{
public:
    virtual Pet& getPat() = 0;
};
