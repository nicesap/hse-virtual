#pragma once

class Animal
{
public:
    virtual ~Animal();

    virtual void interact() = 0;
};
