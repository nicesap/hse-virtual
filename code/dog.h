#pragma once

#include "pet.h"

class Dog final : private Pet
{
    std::string name();
    std::string breed();
};
