#include "pet.h"

#include <cstdio>

Pet::~Pet()
{
    printf("%s was a good pet.\n", name().c_str());
}

void Pet::interact()
{
    printf("My dear %s of the %s breed, come and take meal with me!\n", name().c_str(), breed().c_str());
}
