#include "cat.h"

#include <cstdio>

Cat::Cat(std::string name)
    : mName(name)
{
}

Cat::~Cat()
{
    printf("%s of %s leaves you alone.\n", name().c_str(), breed().c_str());
}

std::string Cat::name() const
{
    return mName;
}

std::string Cat::breed() const
{
    return "SuperKitty2003@WiLlMaKeYoUcRy";
}
