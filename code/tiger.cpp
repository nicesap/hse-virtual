#include "tiger.h"

Tiger::Tiger(std::string name)
    : Cat{name}
{
}

std::string Tiger::breed() const
{
    return "TIGERALLMIGHTY";
}
