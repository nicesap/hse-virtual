#pragma once

#include "animal.h"

#include <string>

class Pet : public Animal
{
public:
    ~Pet() override;

    void interact() final;

protected:
    virtual std::string name() const = 0;
    virtual std::string breed() const = 0;
};
