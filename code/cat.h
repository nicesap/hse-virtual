#pragma once

#include "pet.h"

class Cat : public Pet
{
public:
    explicit Cat(std::string name);
    ~Cat();

private:
    std::string name() const override;
    std::string breed() const override;

    std::string mName;
};
