target/bitset: code/*.h code/*.cpp | target
	g++ -fsanitize=address --std=c++17 code/*.cpp -o target/example

target:
	mkdir target

clean:
	rm -rf target
